FROM python:alpine
MAINTAINER Stein van Broekhoven <stein@aapjeisbaas.nl>

WORKDIR /app/
COPY cleanup.py /app/cleanup.py
RUN pip install requests python-dateutil

ENTRYPOINT ["python", "/app/cleanup.py"]
