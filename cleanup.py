#!/bin/env python3

import requests
import os
import dateutil.parser
import datetime
import sys


# log currnt timestamp
print(datetime.datetime.now())

if 'API_KEY' and 'RADARR_URI' and 'RETENTION' not in os.environ:
    print('Not All required env vars found, we need: API_KEY RADARR_URI RETENTION')
    print('Found env vars:')
    for env_var in os.environ:
        print('{}={}'.format(env_var, os.environ[env_var]))
    sys.exit(1)


api_key = os.getenv("API_KEY")
radarr_uri = os.getenv("RADARR_URI")
retention = int(os.getenv("RETENTION"))


s = requests.Session()
s.headers.update({"accept": "application/json"})

movies = s.get(radarr_uri + "/api/v3/movie" + "?apikey=" + api_key).json()

unmonitored = [movie for movie in movies if movie["monitored"] == False]

for movie in unmonitored:
    try:
        # datetime movie is written to disk
        added = dateutil.parser.parse(movie["movieFile"]["dateAdded"])
    except:
        # datetime movie is added to wanted list
        added = dateutil.parser.parse(movie["added"])
    delete_date = added + datetime.timedelta(days=retention)
    if datetime.date.today() > delete_date.date():
        print("Purge: {}".format(movie["title"]))
        s.delete(
            radarr_uri
            + "/api/v3/movie/"
            + str(movie["id"])
            + "?addImportExclusion=false&deleteFiles=true&apikey="
            + api_key
        )
    else:
        try:
            # this is in a try because the string format failed in the past
            print(
                "Keeping: {} until: {}".format(
                    movie["title"], (delete_date + datetime.timedelta(days=1)).date()
                )
            )
        except:
            pass

# end current pass in the logs with a neat line sepperation
print("------------------------------------------------")
